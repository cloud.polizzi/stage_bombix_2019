import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import matplotlib.patches as patches
import matplotlib.path as path
import matplotlib.colors as mcolors
import scipy as s
import h5py 
import numpy as np
import matplotlib.pyplot as plt
import tables
from scipy.stats import shapiro
from scipy.stats import norm
from math import floor 

#use var=pd.read_csv('File_Name.csv') to open a csv file in the variable var
data=pd.read_csv('tdoa_sess_4.csv')#
data['click_time']=pd.to_datetime(data['click_time'])

#Print simple data (1day with date)
##Set Date
yr=2017
mth=8
da=30


cc1=data.loc[(data['click_time']>='%d-%d-%d 00:00:00.0'%(yr,mth,da))
             &(data['click_time']<'%d-%d-%d 00:00:00'%(yr,mth,da+1))
             #&(data['ipi']>0.0048)&(data['ipi']<0.0055)

             ]

##Plot
plt.plot(cc1['click_time'],cc1['tdoa'],'.')
plt.subplots_adjust(top=1, bottom=0, left=0, right=2, hspace=0.25,wspace=0.15)
plt.title("trace %d/08"%(da))
plt.xlabel('Date')
plt.ylabel('delay')
plt.ylim([-0.0013,0.0013])
plt.xlim(['2017-08-%d 00:00:00.0'%(da),'2017-08-%d 00:00:00'%(da+1)])


#On cherche à sélectionner les données qui posèdent une densité de 'clic détecté' assez haut(limite à définir). 
#Les bruits de fond seront éliminés (plus ou moins), et les traces plus lisibles.

#%%#
#Uniquement pour test = limiter la quantité de données à calculer à un jour. Sinon garder la ligne suivante en commentaire.
# data=cc1.reset_index(drop = True)
#%%#

#on fixe les variables de temps au T0 des données (on peut aussi fixer les dates de départ et de fin)
d0=data.loc[0]['click_time']                                     #On prend la date de la première détection
T0=pd.to_datetime('%d-%d-%d 00:00:00'%(d0.year,d0.month,d0.day)) #On modifie l'heure à 00:00:00 et on le met dans T0
Tt=data.loc[data.shape[0]-1]['click_time']                       #On prend la dernière détection 

dt20=pd.to_timedelta('00:20:00')      #à chaque tour de boucle on avance T0 de 20 minutes
sep=0.00001                           #taille des segments de TDOA en seconde a:0.0001  b:0.00001
TDOAsep=np.arange(-0.0013,0.0013,sep) #on sépare la gamme des TDOA en unités de 0.0001s
Dlim=0.06                             #Densité minimale pour que les points soient validés a:0.15~0.25  b:0.06
data2=pd.DataFrame()                  #Initialisation du dataframe vide pour les données filtrées

#Boucle
for i in np.arange(T0,Tt,dt20):#i allant de T0 à Tt par increments de 20 minutes
    dtemp1=data.loc[(data['click_time']>=i)
                    &(data['click_time']<i+dt20)
                    ]#On isole data entre i et i+20mins
    
    if dtemp1.shape[0]>0:
        nbtotal=dtemp1.shape[0]#Nombre total de clic dans le dataframe (nb de lignes)

        for j in TDOAsep:
            dtemp2=dtemp1.loc[(dtemp1['tdoa']>=j)
                           &(dtemp1['tdoa']<=j+sep)
                          ]#On isole data  dans l'interval de TDOA entre j et j+sep dans dtemp
            
            if dtemp2.shape[0]>0:
                D=(dtemp2.shape[0])/nbtotal#calcul de la densité de pts dan l'intelval de TDOA donné par rapport au nb total de pts
                if D>Dlim:
                    data2=pd.concat([data2,dtemp2], ignore_index = True)

#Affichage pour verification
ccc1=data2.loc[(data2['click_time']>='2017-08-%d 00:00:00.0'%(da))
              &(data2['click_time']<'2017-08-%d 00:00:00'%(da+1))
             ]
ccc1=ccc1.reset_index(drop=True)
plt.plot(ccc1['click_time'],ccc1['tdoa'],'.')
plt.subplots_adjust(top=1, bottom=0, left=0, right=2, hspace=0.25,wspace=0.15)
plt.title("trace %d/08"%(da))
plt.xlabel('Date')
plt.ylabel('delay')
plt.ylim([-0.0013,0.0013])
plt.xlim(['2017-08-%d 00:00:00.0'%(da),'2017-08-%d 00:00:00'%(da+1)])
plt.grid()


##Save new set of data

data2.to_csv('data2.csv')